/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connect.four.player;

import connect.four.board.ReadWritableBoard;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import connect.four.Game;
import connect.four.board.*;
import connect.four.player.*;

/**
 *
 * @author Worm
 */
public class ComputerPlayerTest {
	
	public ComputerPlayerTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of getName method, of class ComputerPlayer.
	 */
	@Test
	public void testGetName() {
		
		
		ComputerPlayer pC = new ComputerPlayer();
		
		assertTrue(pC.getName() == "Computer");
		
	}

	/**
	 * Test of performPlay method, of class ComputerPlayer.
	 */
	@Test
	public void testPerformPlay() {
		
		Board board1 = new Board(7,7);
		Board board2 = new Board(7,7);
		ComputerPlayer pC1 = new ComputerPlayer();
		ComputerPlayer pC2 = new ComputerPlayer();
		
		pC1.performPlay(board1);
		
		assertTrue(board1.getMoveCount() == 1);
		assertTrue(board2.getMoveCount() == 0);
		
	}
	
}
