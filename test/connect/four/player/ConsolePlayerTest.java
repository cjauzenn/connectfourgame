/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connect.four.player;

import connect.four.ScoreChart;
import connect.four.board.ReadWritableBoard;
import connect.four.board.ReadableBoard;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import connect.four.Game;
import connect.four.board.*;
import connect.four.player.*;


/**
 *
 * @author Worm
 */
public class ConsolePlayerTest {
	
	public ConsolePlayerTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of getName method, of class ConsolePlayer.
	 */
	@Test
	public void testGetName() {
		
		Board board1 = new Board(7,7);
		ConsolePlayer p1 = new ConsolePlayer("Player 1");
		ConsolePlayer p2 = new ConsolePlayer("Player 2");
		
		assertTrue(p1.getName().equals("Player 1"));
		assertTrue(p2.getName().equals("Player 2"));
		
	}

	/**
	 * Test of setName method, of class ConsolePlayer.
	 */
	@Test
	public void testSetName() {
		
		ConsolePlayer p1 = new ConsolePlayer("Player 1");
		ConsolePlayer p2 = new ConsolePlayer("Player 2");
		
		p1.setName("Jerry");
		p2.setName("Richard");
		
		assertTrue(p1.getName().equals("Jerry"));
		assertTrue(p2.getName().equals("Richard"));
		
	}

	/**
	 * Test of gameOver method, of class ConsolePlayer.
	 */
	@Test
	public void testGameOver() {
		
						
		
	}

	/**
	 * Test of performPlay method, of class ConsolePlayer.
	 */
	@Test
	public void testPerformPlay() {
		
		Board board1 = new Board(7,7);
		Board board2 = new Board(7,7);
		ConsolePlayer p1 = new ConsolePlayer("@");
		ConsolePlayer p2 = new ConsolePlayer("X");
		
		p1.performPlay(board1);
		p2.performPlay(board1);
		
		assertTrue(board1.getMoveCount() == 2);
		assertTrue(board2.getMoveCount() == 0);
		
}
	
}
