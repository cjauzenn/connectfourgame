/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connect.four;

import connect.four.board.ReadableBoard;
import connect.four.player.Player;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Test;
import connect.four.Game;
import connect.four.board.*;
import connect.four.player.*;

/**
 *
 * @author Worm
 */
public class GameTest {
	
	public GameTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of start method, of class Game.
	 */
	@Test
	public void testStart() {
		System.out.println("start");
		Game instance = null;
		instance.start();
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of registerListener method, of class Game.
	 */
	@Test
	public void testRegisterListener() {
		System.out.println("registerListener");
		ScoreChart.Listener l = null;
		Game instance = null;
		instance.registerListener(l);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of unregisterListener method, of class Game.
	 */
	@Test
	public void testUnregisterListener() {
		System.out.println("unregisterListener");
		ScoreChart.Listener l = null;
		Game instance = null;
		instance.unregisterListener(l);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of getPlayers method, of class Game.
	 */
	@Test
	public void testGetPlayers() {
		System.out.println("getPlayers");
		Game instance = null;
		List<Player> expResult = null;
		List<Player> result = instance.getPlayers();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of getScore method, of class Game.
	 */
	@Test
	public void testGetScore() {
		System.out.println("getScore");
		Player p = null;
		Game instance = null;
		int expResult = 0;
		int result = instance.getScore(p);
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of performPlay method, of class Game.
	 */
	@Test
	public void testPerformPlay() {
		System.out.println("performPlay");
		int player = 0;
		Game instance = null;
		instance.performPlay(player);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of getCurrentPlayer method, of class Game.
	 */
	@Test
	public void testGetCurrentPlayer() {
		System.out.println("getCurrentPlayer");
		Game instance = null;
		Player expResult = null;
		Player result = instance.getCurrentPlayer();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of getInRow method, of class Game.
	 */
	@Test
	public void testGetInRow() {
		System.out.println("getInRow");
		Game instance = null;
		int expResult = 0;
		int result = instance.getInRow();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of getBoard method, of class Game.
	 */
	@Test
	public void testGetBoard() {
		System.out.println("getBoard");
		Game instance = null;
		ReadableBoard expResult = null;
		ReadableBoard result = instance.getBoard();
		assertEquals(expResult, result);
		// TODO review the generated test code and remove the default call to fail.
		fail("The test case is a prototype.");
	}

	/**
	 * Test of detectWinner method, of class Game.
	 */
	@Test
	public void testDetectWinner() {
		
		/**
		*Initialize how many pieces in a row to win
		*/
		int inRow = 4;
		
		/**
		*Initialize players with @ or X coins.
		*/
		ConsolePlayer p1 = new ConsolePlayer("@");
		ConsolePlayer p2 = new ConsolePlayer("X");
		
		/**
		 * Sets up six boards layouts
		 */
		Board board1 = new Board(7,7);
		Board board2 = new Board(7,7);
		Board board3 = new Board(7,7);
		Board board4 = new Board(7,7);
		Board board5 = new Board(7,7);
		Board board6 = new Board(7,7);
		
		
		/**
		 * board 1
		 * No moves made, no winner
		 */
		
		/**
		 * board 2
		 * p1 Horizontal win
		 */
		board2.play(0, p2);
		board2.play(0, p1);
		board2.play(0, p2);
		board2.play(0, p1);
		board2.play(2, p2);
		board2.play(1, p1);
		board2.play(1, p2);
		board2.play(1, p1);
		board2.play(2, p2);
		board2.play(1, p1);
		board2.play(2, p2);
		board2.play(2, p1);
		board2.play(3, p2);
		board2.play(3, p1);
		board2.play(3, p2);
		board2.play(3, p1);
		
		/**
		 * board 3
		 * p2 Vertical win
		 */
		board3.play(0, p2);
		board3.play(1, p1);
		board3.play(0, p2);
		board3.play(1, p1);
		board3.play(0, p2);
		board3.play(2, p1);
		board3.play(0, p2);
		
		/**
		 * board 4
		 * p1 Diagonal win
		 */
		board4.play(0, p1);
		board4.play(1, p2);
		board4.play(1, p1);
		board4.play(2, p2);
		board4.play(3, p1);
		board4.play(2, p2);
		board4.play(2, p1);
		board4.play(3, p2);
		board4.play(3, p1);
		board4.play(0, p2);
		board4.play(3, p1);
		

		/**
		 * board 5
		 * p2 Diagonal win
		 */
		board5.play(3, p2);
		board5.play(2, p1);
		board5.play(2, p2);
		board5.play(1, p1);
		board5.play(0, p2);
		board5.play(1, p1);
		board5.play(1, p2);
		board5.play(0, p1);
		board5.play(0, p2);
		board5.play(3, p1);
		board5.play(0, p2);
		
		/**
		 * board 6
		 * No Winner yet
		 */
		board6.play(0, p1);
		board6.play(0, p2);
		board6.play(0, p1);
		board6.play(1, p2);
		board6.play(1, p1);
		board6.play(1, p2);
		board6.play(2, p1);
		board6.play(2, p2);
		board6.play(2, p1);
		board6.play(3, p2);
		board6.play(3, p1);
		board6.play(3, p2);
		board6.play(4, p1);
		board6.play(4, p2);
		board6.play(4, p1);
		board6.play(5, p2);
		board6.play(5, p1);
		board6.play(5, p2);
		board6.play(6, p1);
		board6.play(6, p2);
		board6.play(6, p1);
		
				
		//Test board1 - no moves made
		assertNull(Game.detectWinner(board1, inRow));
		//Test board2 - p1 horizontal win
		assertEquals(p1, Game.detectWinner(board2, inRow));
		//Test board3 - p2 vertical win
		assertEquals(p2, Game.detectWinner(board3, inRow));
		//Test board4 - p1 diagonal win
		assertEquals(p1, Game.detectWinner(board4, inRow));
		//Test board5 - p2 diagonal win
		assertEquals(p2, Game.detectWinner(board5, inRow));
		//Test board6 - Moves made, no winner
		assertNull(Game.detectWinner(board6, inRow));
			
		
		
	}
	
}
