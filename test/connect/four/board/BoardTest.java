/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connect.four.board;

import connect.four.player.Player;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import connect.four.Game;
import connect.four.board.*;
import connect.four.player.*;

/**
 *
 * @author Worm
 */
public class BoardTest {
	
	public BoardTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of whoPlayed method, of class Board.
	 */
	@Test
	public void testWhoPlayed() {
		
		//Initialize board and players
		
		Board board1 = new Board(7,7);
		
		ConsolePlayer p1 = new ConsolePlayer("@");
		ConsolePlayer p2 = new ConsolePlayer("X");
		
		//make moves on board with players
		board1.play(0, p2);
		board1.play(1, p1);
		board1.play(1, p2);
		board1.play(2, p2);
		board1.play(2, p1);
		board1.play(2, p2);
		board1.play(3, p2);
		board1.play(3, p2);
		board1.play(3, p2);
		board1.play(3, p2);
		board1.play(4, p2);
		board1.play(4, p2);
		board1.play(4, p2);
		board1.play(4, p2);
		board1.play(4, p2);
		board1.play(5, p1);
		board1.play(5, p2);
		board1.play(5, p2);
		board1.play(5, p2);
		board1.play(5, p2);
		board1.play(5, p2);
		board1.play(6, p2);
		board1.play(6, p2);
		board1.play(6, p2);
		board1.play(6, p2);
		board1.play(6, p2);
		board1.play(6, p2);
		board1.play(6, p1);
		
		//test spot played matches player
		assertTrue(board1.whoPlayed(0,0).equals(p2));
		assertTrue(board1.whoPlayed(1, 1).equals(p2));
		assertTrue(board1.whoPlayed(2, 1).equals(p1));
		assertTrue(board1.whoPlayed(3, 3).equals(p2));
		assertTrue(board1.whoPlayed(4, 2).equals(p2));
		assertTrue(board1.whoPlayed(5, 0).equals(p1));
		assertTrue(board1.whoPlayed(6, 6).equals(p1));
		
	}

	/**
	 * Test of getWidth method, of class Board.
	 */
	@Test
	public void testGetWidth() {
		
		//Initialize boards
		Board board1 = new Board(5,6);
		Board board2 = new Board(0, 4);
		
		//Test board widths
		assertTrue(board1.getWidth() == 5);
		assertTrue(board2.getWidth() == 0);
	
	}

	/**
	 * Test of getHeight method, of class Board.
	 */
	@Test
	public void testGetHeight() {
		
		//Initialize boards
		Board board1 = new Board(5,6);
		Board board2 = new Board(4, 0);
		
		//Test board heights
		assertTrue(board1.getHeight() == 6);
		assertTrue(board2.getHeight() == 0);
		
	}

	/**
	 * Test of play method, of class Board.
	 */
	@Test
	public void testPlay() {
		
		//Initialize board and players
		Board board1 = new Board(7,7);
		
		ConsolePlayer p1 = new ConsolePlayer("@");
		ConsolePlayer p2 = new ConsolePlayer("X");
		
		//Play() moves on board
		board1.play(0, p1);
		board1.play(0, p2);
		board1.play(1, p1);
		board1.play(1, p2);
		board1.play(1, p1);
		board1.play(2, p2);
		board1.play(2, p1);
		board1.play(2, p2);
		board1.play(4, p1);
		board1.play(5, p2);
		board1.play(5, p2);
		board1.play(6, p1);
		board1.play(6, p1);
		
		//Test that moves were made to board
		assertTrue(board1.whoPlayed(0, 1).equals(p2));
		assertTrue(board1.whoPlayed(6, 1).equals(p1));
		
	}

	/**
	 * Test of getColumnHeight method, of class Board.
	 */
	@Test
	public void testGetColumnHeight() {
		
		//Initialize board and players
		Board board1 = new Board(7,7);
		
		ConsolePlayer p1 = new ConsolePlayer("@");
		ConsolePlayer p2 = new ConsolePlayer("X");
		
		//Make moves to create different column heights
		board1.play(0, p1);
		board1.play(0, p2);
		board1.play(1, p1);
		board1.play(1, p2);
		board1.play(1, p1);
		board1.play(2, p2);
		board1.play(2, p1);
		board1.play(2, p2);
		board1.play(4, p1);
		board1.play(5, p2);
		board1.play(5, p2);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		
		//Test column heights
		assertTrue(board1.getColumnHeight(0) == 2);
		assertTrue(board1.getColumnHeight(3) == 0);
		assertTrue(board1.getColumnHeight(6) == 7);
		
	}

	/**
	 * Test of clear method, of class Board.
	 */
	@Test
	public void testClear() {
		
		//Initialize board and players
		Board board1 = new Board(7,7);
		
		ConsolePlayer p1 = new ConsolePlayer("@");
		ConsolePlayer p2 = new ConsolePlayer("X");
		
		//Make a number of moves on board
		board1.play(0, p1);
		board1.play(0, p2);
		board1.play(1, p1);
		board1.play(1, p2);
		board1.play(1, p1);
		board1.play(2, p2);
		board1.play(2, p1);
		board1.play(2, p2);
		board1.play(4, p1);
		board1.play(5, p2);
		board1.play(5, p2);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		
		//Test that moves worked
		assertTrue(board1.getColumnHeight(0) == 2);
		assertTrue(board1.getColumnHeight(3) == 0);
		assertTrue(board1.getColumnHeight(6) == 7);
		
		//clear the board
		board1.clear();
		
		//Test that columns are clear
		assertTrue(board1.getColumnHeight(0) == 0);
		assertTrue(board1.getColumnHeight(3) == 0);
		assertTrue(board1.getColumnHeight(6) == 0);
		
	}

	/**
	 * Test of getMoveCount method, of class Board.
	 */
	@Test
	public void testGetMoveCount() {
		
		//Initialize boards and players
		Board board1 = new Board(7,7);
		Board board2 = new Board(7,7);
		Board board3 = new Board(7,7);
		Board board4 = new Board(7,7);
		
		ConsolePlayer p1 = new ConsolePlayer("@");
		ConsolePlayer p2 = new ConsolePlayer("X");
		
		//Make a number of moves on board1(18 moves)
		board1.play(0, p1);
		board1.play(0, p2);
		board1.play(1, p1);
		board1.play(1, p2);
		board1.play(1, p1);
		board1.play(2, p2);
		board1.play(2, p1);
		board1.play(2, p2);
		board1.play(4, p1);
		board1.play(5, p2);
		board1.play(5, p2);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		board1.play(6, p1);
		
		//Make a number of moves on board2(13 moves)
		board2.play(0, p1);
		board2.play(0, p2);
		board2.play(1, p1);
		board2.play(1, p1);
		board2.play(2, p2);
		board2.play(2, p1);
		board2.play(4, p1);
		board2.play(5, p2);
		board2.play(5, p2);
		board2.play(6, p1);
		board2.play(6, p1);
		board2.play(6, p1);
		board2.play(6, p1);
		
		//No moves made on board3
		
		//Fill board4 with moves
		board4.play(0, p1);
		board4.play(0, p2);
		board4.play(0, p1);
		board4.play(0, p2);
		board4.play(0, p1);
		board4.play(0, p2);
		board4.play(0, p2);
		board4.play(1, p1);
		board4.play(1, p2);
		board4.play(1, p1);
		board4.play(1, p2);
		board4.play(1, p2);
		board4.play(1, p1);
		board4.play(1, p1);
		board4.play(2, p1);
		board4.play(2, p1);
		board4.play(2, p1);
		board4.play(2, p1);
		board4.play(2, p1);
		board4.play(2, p1);
		board4.play(2, p1);
		board4.play(3, p1);
		board4.play(3, p2);
		board4.play(3, p1);
		board4.play(3, p1);
		board4.play(3, p2);
		board4.play(3, p1);
		board4.play(3, p1);
		board4.play(4, p1);
		board4.play(4, p2);
		board4.play(4, p2);
		board4.play(4, p1);
		board4.play(4, p1);
		board4.play(4, p1);
		board4.play(4, p1);
		board4.play(5, p1);
		board4.play(5, p1);
		board4.play(5, p1);
		board4.play(5, p1);
		board4.play(5, p1);
		board4.play(5, p1);
		board4.play(5, p1);
		board4.play(6, p2);
		board4.play(6, p1);
		board4.play(6, p1);
		board4.play(6, p2);
		board4.play(6, p1);
		board4.play(6, p1);
		board4.play(6, p2);
		
		//Test MoveCount matched number of moves made
		assertTrue(board1.getMoveCount() == 18);
		assertTrue(board2.getMoveCount() == 13);
		assertTrue(board3.getMoveCount() == 0);
		assertTrue(board4.getMoveCount() == 49);
		
	}
	
}
