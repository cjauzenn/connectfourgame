/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connect.four.gui;

import connect.four.Game;
import connect.four.ScoreChart;
import connect.four.board.Board;
import connect.four.board.ReadWritableBoard;
import connect.four.board.ReadableBoard;
import connect.four.player.Player;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pmmu_000
 */
public class GamePanelTest {
	
	public GamePanelTest() {
	}
	
	@BeforeClass
	public static void setUpClass() {
	}
	
	@AfterClass
	public static void tearDownClass() {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

//	/**
//	 * Test of dropPiece method, of class GamePanel.
//	 */
//	@Test
//	public void testDropPiece() {
//		System.out.println("dropPiece");
//		GamePanel instance = null;
//		instance.dropPiece();
//	}
//
//	/**
//	 * Test of getTargetY method, of class GamePanel.
//	 */
//	@Test
//	public void testGetTargetY() {
//		System.out.println("getTargetY");
//		GamePanel instance = null;
//		int expResult = 0;
//		int result = instance.getTargetY();
//		assertEquals(expResult, result);
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of isValidMove method, of class GamePanel.
//	 */
//	@Test
//	public void testIsValidMove() {
//		System.out.println("isValidMove");
//		GamePanel instance = null;
//		boolean expResult = false;
//		boolean result = instance.isValidMove();
//		assertEquals(expResult, result);
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of getColumnNum method, of class GamePanel.
//	 */
//	@Test
//	public void testGetColumnNum() {
//		System.out.println("getColumnNum");
//		GamePanel instance = null;
//		int expResult = 0;
//		int result = instance.getColumnNum();
//		assertEquals(expResult, result);
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of calcWidth method, of class GamePanel.
//	 */
//	@Test
//	public void testCalcWidth() {
//		System.out.println("calcWidth");
//		int columnEntered = 0;
//		GamePanel instance = null;
//		instance.calcWidth(columnEntered);
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of calcNewPos method, of class GamePanel.
//	 */
//	@Test
//	public void testCalcNewPos() {
//		System.out.println("calcNewPos");
//		int columnEntered = 0;
//		GamePanel instance = null;
//		instance.calcNewPos(columnEntered);
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of turnUp method, of class GamePanel.
//	 */
//	@Test
//	public void testTurnUp() {
//		System.out.println("turnUp");
//		GamePanel instance = null;
//		instance.turnUp();
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of turn method, of class GamePanel.
//	 */
//	@Test
//	public void testTurn() {
//		System.out.println("turn");
//		GamePanel instance = null;
//		instance.turn();
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of initNewGame method, of class GamePanel.
//	 */
//	@Test
//	public void testInitNewGame() {
//		System.out.println("initNewGame");
//		GamePanel instance = null;
//		instance.initNewGame();
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}
//
//	/**
//	 * Test of globalGlow method, of class GamePanel.
//	 */
//	@Test
//	public void testGlobalGlow() {
//		System.out.println("globalGlow");
//		GamePanel instance = null;
//		instance.globalGlow();
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}

	/**
	 * Test of gameOver method, of class GamePanel.
	 */
	@Test
	public void testGameOver() {
		//arrange
		Player[] players = new Player[2];
		ReadWritableBoard board = new Board(6, 7);
		ScoreChart scores = new Game(players, board, 4);
		ReadableBoard end = null;
		GamePanel instance = new GamePanel(new GUI(), true);
		instance.turnNum = 41;
		
		//act
		instance.gameOver(null, scores, end);
		
		//assert
		assertEquals("It's a tie!", instance.gui.winner);
	}
//
//	/**
//	 * Test of glow method, of class GamePanel.
//	 */
//	@Test
//	public void testGlow() {
//		System.out.println("glow");
//		GUIPiece currentPiece = null;
//		GamePanel instance = null;
//		instance.glow(currentPiece);
//		// TODO review the generated test code and remove the default call to fail.
//		fail("The test case is a prototype.");
//	}
	
}
