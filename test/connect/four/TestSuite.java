package connect.four;

import org.junit.runners.Suite.SuiteClasses;
import org.junit.runners.Suite;
import org.junit.runner.RunWith;

import connect.four.board.BoardTest;
import connect.four.player.ComputerPlayerTest;
import connect.four.player.ConsolePlayerTest;

/**
 * Test suite to run all tests in the lab together
 * @author Clayton Auzenne Jr.
 */
@RunWith(Suite.class)
@SuiteClasses({ BoardTest.class,
	ComputerPlayerTest.class, ConsolePlayerTest.class })

public class TestSuite { }
